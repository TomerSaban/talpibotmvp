import random
import uuid
from abc import ABC
from typing import List, ClassVar

from TalpiWeb_MVP import views


class UI_Component(ABC):
    def __init__(self):
        self.id = None

    def render(self):
        pass

    def method_to_url(self, method_id):
        return f'/run_func/?method_id={method_id}'

    def submit_to_url(self, method_id):
        return f'/get_data/?method_id={method_id}'

    def encapsulate_submit(self, f, object_type, obj):
        if not f:
            return None
        else:
            # TODO: Implement using action lst instead of direct return
            def _f(properties):
                obj_class: ClassVar = object_type
                ret_ovb = obj_class(**properties)
                if obj.pk is not None:  # if the object already in the db
                    ret_ovb.pk = obj.pk
                result = f(ret_ovb)
                if result:
                    return result
                else:
                    return {"data": {"value": None}}

            return _f


class Page(ABC):
    def __init__(self):
        self.__components: List[UI_Component] = []
        self.__components_ids: List[str] = []

    def add_component(self, component):
        component_id = self.__generate_id()
        self.__components_ids.append(component_id)
        self.__components.append(component)
        component.id = component_id
        # return component_id

    def get_page_ui(self):
        self.page_ui()
        for comp in self.__components:
            views.actions_lst.append(comp.render())

    def __generate_id(self):
        """
        YOU SHOULD NOT USE THIS METHOD
        """
        comp_id = str(uuid.uuid4())
        self.__components_ids.append(comp_id)
        return comp_id

    def page_ui(self):
        """
        here you should write your code
        """
        pass
