# from general import *
import datetime

from TalpiWeb_MVP.Page import *
from TalpiWeb_MVP.UI_Components import *
from mongoengine import *


class Person(Document):
    name = StringField()
    address = StringField()
    birthday = DateField()


class FIRST_MVP(Page):
    def __init__(self):
        super().__init__()
        self.button1 = None
        self.button2 = None
        self.n = 0

    def do_action(self):
        self.n += 1
        self.button1.update_text("לחצת עלי " + str(self.n) + " פעמים ")

    def blabla(self, obj):
        print(obj.to_mongo(), obj['id'])
        return {'song':'Hallelujah'}

    def page_ui(self):
        n = 0
        self.button1 = Button(text="לחצת עלי 0 פעמים", action=self.do_action)
        self.button2 = Button(text="תהרוג אותי")
        obj = Person(name='John', address='Bla Bla 2', birthday=datetime.date.today())
        # print('original id: ',obj.get(pk=id))
        form1 = Form(obj,
                     editable=['address', 'birthday'],
                     submit = self.blabla,
                     placeholder=['address'],
                     display_name={'name': 'שם', 'address': 'כתובת',
                                   'birthday': 'יום הולדת'})

        self.add_component(self.button1)
        self.add_component(self.button2)
        self.add_component(form1)

