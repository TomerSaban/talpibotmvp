from django.contrib import admin
from django.urls import path
import TalpiWeb_MVP.views as views
import numpy as np

urlpatterns = [
    path('run_func/', views.run_func, name='run_func'),
    path('get_page/', views.get_page, name='get_page'),
    path('get_data/', views.get_data, name='get_data')
]


