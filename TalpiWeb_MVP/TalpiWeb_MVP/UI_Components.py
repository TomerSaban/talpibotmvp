import random
from typing import List

from TalpiWeb_MVP import views
from TalpiWeb_MVP.Page import *


class Button(UI_Component):
    def __init__(self, text="", action=None):
        self.__text = text
        if action:
            func_id = views.get_id(action)
            self.__action = self.method_to_url(func_id)
        else:
            self.__action = None

    def render(self):
        return {
            'action': 'add',
            'value': {'type': 'Button',
                      'id': self.id,
                      'text': self.__text,
                      'action': self.__action}
        }

    def update_text(self, text: str):
        self.__text = text
        views.actions_lst.append({
            'action': 'change',
            'value': {"id": self.id,
                      "text": self.__text}
        })


class Label(UI_Component):
    def __init__(self, page: Page, text=""):
        self.__text = text

    def render(self):
        return {
            'action': 'add',
            'value': {'type': 'Label',
                      'id': self.id,
                      'text': self.__text
                      }
        }

    def update_text(self, text):
        self.__text = text
        views.actions_lst.append({
            'action': 'change',
            'value': {"id": self.id,
                      "text": self.__text}
        })


class Form(UI_Component):
    def __init__(self, obj, visible=[],
                 editable: List = [], display_name={},
                 placeholder: List = [], submit=None):
        self.__obj = obj

        self.__properties = self.__obj_to_dict(obj, visible, editable, display_name, placeholder)

        action = self.encapsulate_submit(submit, type(obj), obj)
        if action:
            func_id = views.get_id(action)
            self.__action = self.submit_to_url(func_id)
        else:
            self.__action = None
        self.visible = visible
        self.editable = editable

    def __obj_to_dict(self, obj, visible, editable, display_name: dict, placeholder):
        obj_dict = obj.to_mongo()
        props = []
        for field in obj_dict:
            name = field
            field_type = type(obj[field]).__name__
            field_display_name = display_name.get(field, name)
            is_editable, is_placeholder, is_visible = True, True, True
            if editable:
                is_editable = field in editable
            if placeholder:
                is_placeholder = field in placeholder
            if visible:
                is_visible = field in visible
            initial_data = obj_dict[field]

            res = {"name": name,
                   "type": field_type,
                   "display_name": field_display_name,
                   "editable": is_editable,
                   "visible": is_visible,
                   "initial_value": initial_data,
                   "is_placeholder": is_placeholder,
                   }
            props.append(res)
        return props

    def render(self):
        return {
            'action': 'add',
            'value': {'type': 'Form',
                      'id': self.id,
                      'properties': self.__properties,
                      'action': self.__action}
        }

    def update_form(self):
        return {
            'action': 'change',
            'value': {'type': 'Form',
                      'id': self.id,
                      'properties': self.__properties,
                      'action': self.__action}
        }
