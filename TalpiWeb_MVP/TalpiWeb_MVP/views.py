import json
import uuid

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from TalpiWeb_MVP.pages.templates import *


actions_lst = []
ids_to_funcs = {}


@csrf_exempt
def get_page(request):
    class_name = request.GET.get("name")
    File_obj = eval(class_name)()
    File_obj.get_page_ui()

    return send_to_server()


def send_to_server():
    List_of_dicts = []
    dict_of_List = {}
    for action in actions_lst:
        List_of_dicts.append(action["value"])
    # print(actions_lst)
    actions_lst.clear()
    dict_of_List["data"] = List_of_dicts
    # print(dict_of_List)
    return good_json_response(dict_of_List)


@csrf_exempt
def get_data(request):
    payload = json.loads(request.body)
    if payload.get('data'):
        method_id = request.GET.get("method_id")
        # print(request.content_params)
        properties = payload['data']["properties"]
        print(f"Data received from {request.get_host()}: \n {payload}")

        # TODO: Implement using action lst instead of direct return
        return good_json_response(ids_to_funcs[method_id](properties))

    else:
        return good_json_response({'data': 'no data in request'})


@csrf_exempt
def good_json_response(data):
    response = JsonResponse(data)
    response['Access-Control-Allow-Origin'] = '*'

    return response


def add_access_token(response):
    response['Access-Control-Allow-Origin'] = '*'
    return response


def get_id(func):
    func_id = uuid.uuid4()
    # print("origianl_id=", func_id)
    ids_to_funcs[str(func_id)] = func
    return func_id


@csrf_exempt
def run_func(request):
    action_id = request.GET.get('method_id')
    print(request.GET)
    if action_id:
    # print(request.GET)
        return run_func_helper(ids_to_funcs[action_id])
    else:
        return good_json_response({"bad method id"})

def run_func_helper(func):
    func()

    dict_of_dicts = {}

    # print("ids to funcs:" ,ids_to_funcs)
    for action in actions_lst:
        dict_of_dicts[action["value"]["id"]] = action

    actions_lst.clear()
    # print(dict_of_dicts)
    return good_json_response(dict_of_dicts)
